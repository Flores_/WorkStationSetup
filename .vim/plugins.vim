" Plugins file 

" NERDTree
Plug 'scrooloose/nerdTree'

" Syntastic
Plug 'scrooloose/syntastic'

" Gruvbox for theming
Plug 'morhetz/gruvbox'

" Lightline as bar
Plug 'itchyny/lightline.vim'

" filetype icons for NERDTree
Plug 'ryanoasis/vim-devicons'

" tagbar for file mapping
Plug 'majutsushi/tagbar'

" Jedi-vim for python autocompletion
Plug 'davidhalter/jedi-vim'

