""""""""""""""
"KEY BINDINGS"
""""""""""""""

" copy and paste
map <C-C> y
map <C-V> p

" easier navigation between split windows
map <c-j> <c-w>j
map <c-k> <c-w>k
map <c-h> <c-w>h
map <c-l> <c-w>l

" NERDTree open an close buffer to navigate filesystem
map <F9> :NERDTreeToggle <CR>

" Tagbar to map the file and naavigate through it
map <F10> :TagbarToggle <CR>

" Syntastic key bindings to perform a check and close the error log console
map <F11> :SyntasticCheck <CR>
map <F12> :SyntasticReset <CR>

