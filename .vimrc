call plug#begin('~/.vim/plugged')

source $HOME/.vim/plugins.vim

call plug#end()

" keybindings
source $HOME/.vim/keybindings.vim


" Formatting
filetype plugin indent on
syntax enable
set completeopt-=preview
set textwidth=78
set encoding=UTF-8
set backspace=indent,eol,start
set mouse=
set tabstop=4
set shiftwidth=4
set softtabstop=4
set expandtab

" Appearance
set t_Co=256
let g:gruvbox_italic=1
colorscheme gruvbox
set background=dark
set cursorline
set number
set laststatus=2
set showcmd
set showtabline=1

" Plugin settings
source $HOME/.vim/pluginsconf.vim

